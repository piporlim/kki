<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'mp_kki' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '?${O+MPy=QAq)s8.oxatB1a-UOKZz]r0qq; vPb5<x^Xzr}MI;;rD$&qqjLmeo8?' );
define( 'SECURE_AUTH_KEY',  '8U_ 91pobB|gn1.h>Cri?cZL48^YZI,;N<Zz%3PX+)]x)J<*:n1m!5(De&+R#):[' );
define( 'LOGGED_IN_KEY',    '?iXuFBM0@$QwoR=Rx|R[L[F~#~1}W+|=JWu;X]K-3y4P2nwc|U7`X4jR@?y)-,o1' );
define( 'NONCE_KEY',        'qxoG:UOH;@C@-K>ivYyFO]]]$Xs1<_J@Dp]/{|~Ykx4;<G>h+IC{wLiZWamz#)w$' );
define( 'AUTH_SALT',        'wF_ fw`/P+{5@8X8!:3v<Xv*|-r=2BalRr|pUX!`t)7bW[{a%no_Q]LbET7:fWT+' );
define( 'SECURE_AUTH_SALT', '|;>sQ|Tcd3(MC,0fNFr_Eu%gq7,K2Y]3gSS(B0kA^>K4l/Z?D(mN,k3k@OUMe&Bd' );
define( 'LOGGED_IN_SALT',   'K<e03VWdw4A]%xj?BYL`{GMtc,`Vcil1gC.k;NY<BlJ1$Q|%@q_;9,F.R-j]xi/Y' );
define( 'NONCE_SALT',       '@MeaK}Ab)|jAAky>X=HL6!t:[:@0Q!u^]iAn R_T;ONv:yZ4ZK{3,c8>%)n1.vC/' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
