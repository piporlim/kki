<div class="wrap-place-order">
	<div class="container">
            <div class="section-content">
                <div class="order-form">
                    <div class="order-form-control">
                        <h4> <?php echo get_field('order-title'); ?></h4>
                        <div class="order">
                            <?php echo do_shortcode('[contact-form-7 id="100" title="contact-3"]')?> 
                        </div>
                    </div>
                </div>
                <div class="right-img">
                    <div class="img-control">
                        <img src="<?php the_field('order-img'); ?>">
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>