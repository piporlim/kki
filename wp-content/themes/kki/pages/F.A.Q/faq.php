<div class="faq">
    <div class="container">
        <div class="section-faq">
            <div class="faq-control">
                <?php
                    // Check rows exists.
                    if( have_rows('faq') ):

                        // Loop through rows.
                        while( have_rows('faq') ) : the_row();

                            // Load sub field value.
                            $title = get_sub_field('faq-title');
                            $description = get_sub_field('faq-description');
                            // Do something... 
                            ?>
                            <ul id="accordion">
                                
                                <li>
                                    <h3><?php echo $title?></h3>
                                    <div class="answer">
                                        <p><?php echo $description?></p>
                                    </div>
                                </li>
                            </ul>

                    <?php    // End loop.
                        endwhile;

                    // No value.
                    ?>
                <?php    else :
                        // Do something...
                    endif;
                ?>    

            </div>
        </div>

    </div>
</div>