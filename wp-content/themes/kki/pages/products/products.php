<?php $args = array(
    'post_type'=> 'product',
    'orderby'    => 'ID',
    'post_status' => 'publish',
    'order'    => 'ASC',
    'posts_per_page'    => -1,
);
$query = new WP_Query( $args ); 
?>														
<?php if( $query && $query->have_posts() ): ?>
    <div class="container">
        <div class="products-grid">
            <?php while( $query->have_posts() ): $query->the_post(); ?>
                <div class="grid-item">
                    <div class="grid-item-inner">
                        <div class="products">
                            <div class="img-control">
                                <?php if ( has_post_thumbnail() ) : ?>
                                    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                        <?php the_post_thumbnail(); ?>
                                    </a>
                                    <div class="title-control">
                                        <h3><?php the_title(); ?></h3>
                                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                            <p class="read-more">
                                                <span>Узнать больше</span>
                                                <span class="icon" ></span>
                                            </p>
                                        </a>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <!-- <div class="image" style="background-image: url(<?php //echo get_the_post_thumbnail_url(); ?>);"></div> -->
                        </div>
                    </div>
                </div>
            <?php endwhile; wp_reset_postdata(); ?>
        </div>
    </div>
<?php endif; ?>