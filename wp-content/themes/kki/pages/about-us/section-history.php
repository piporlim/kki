<div class="aboutus-history">
	<div class="container">
		<div class="section-history">
			<h2><?php the_field('section_history'); ?></h2>
		</div>
		<div class="history">
			<?php if( have_rows('images_repeater') ): ?>
			<div class="history-img">
				<?php while ( have_rows('images_repeater') ) : the_row(); ?>
					<img src="<?php the_sub_field('images'); ?>">
				<?php endwhile; ?>
			</div>	
			<?php endif; ?>
			<div class="history-detail">
				<h3><?php the_field('history_title'); ?></h3>
				<?php if( have_rows('history_description') ): ?>
					<div class="history-description">
						<?php while ( have_rows('history_description') ) : the_row(); ?>
							<div class="histories">
								<p><?php the_sub_field('description_line'); ?></p>
							</div>
						<?php endwhile; ?>
					</div>	
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>