<div class="aboutus-keypoint">
	<div class="container">
		<div class="section-keypoint">
			<div class="title">
				<h2><?php the_field('section_keypoint'); ?></h2>
			</div>
			<div class="keypoint-detail">
				<?php if( have_rows('keypoint_repeater') ): ?>
					<div class="keypoint-row">
					<?php while ( have_rows('keypoint_repeater') ) : the_row(); ?>
						<div class="keypoint-list">
							<div class="keypoint">
								<span class="oval"></span>
								<div class="info">
									<h3><?php the_sub_field('year'); ?></h3>
									<p><?php the_sub_field('description'); ?></p>
								</div>
							</div>
						</div>	
					<?php endwhile; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>