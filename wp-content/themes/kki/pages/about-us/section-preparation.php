<div class="aboutus-preparation">
	<div class="container">
		<div class="section-preparation">
			<h2><?php the_field('section_preparation'); ?></h2>
		</div>
		<div class="preparation">
			<div  class="video">
				<video controls poster="<?php the_field('video_thumnail'); ?>" src="<?php the_field('preparation_video'); ?>"></video>
			</div>
			<div class="preparation-des">
				<h3><?php the_field('preparation_title'); ?></h3>
				<p><?php the_field('preparation_description'); ?></p>
			</div>
		</div>
	</div>
</div>