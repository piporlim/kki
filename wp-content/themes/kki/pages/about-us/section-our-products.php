<div class="aboutus-our-products" style="background-image: url('<?php the_field('product_bg'); ?>');">
	<div class="container">
		<div class="section-our-products">
			<div class="title">
				<h2><?php the_field('section_our_products'); ?></h2>
			</div>
			<div class="products-detail">
				<?php if( have_rows('products_details') ): ?>
					<div class="product-row">
					<?php while ( have_rows('products_details') ) : the_row(); ?>
						<div class="products-list">
							<div class="product">
								<img src="<?php the_sub_field('products_img'); ?>">
							</div>
							<div class="product">
								<p><?php the_sub_field('rus_description'); ?></p>
								<p><?php the_sub_field('eng_description'); ?></p>
							</div>
						</div>	
					<?php endwhile; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>