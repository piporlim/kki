<div class="aboutus-quality">
	<div class="container">
		<div class="section-quality">
			<div class="title">
				<h2><?php the_field('section_quality'); ?></h2>
			</div>
			<div class="quality-detail">
				<?php if( have_rows('quality_repeater') ): ?>
					<div class="quality-row">
					<?php while ( have_rows('quality_repeater') ) : the_row(); ?>
						<div class="quality-list">
							<div class="quality">
								<img src="<?php the_sub_field('logo'); ?>">
							</div>
							<div class="quality-des">
								<h3><?php the_sub_field('title'); ?></h3>
								<p><?php the_sub_field('description'); ?></p>
							</div>
						</div>	
					<?php endwhile; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>