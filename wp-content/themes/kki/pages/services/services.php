<?php $args = array(
    'post_type'=> 'service',
    'orderby'    => 'ID',
    'post_status' => 'publish',
    'order'    => 'ASC',
    'posts_per_page'    => -1,
);
$query = new WP_Query( $args ); 
?>                                                      
<?php if( $query && $query->have_posts() ): ?>
    <div class="grid">
        <?php while( $query->have_posts() ): $query->the_post(); ?>
            <div class="grid-item">
                <div class="grid-item-inner">
                    <div class="services">
                        <div class="img-control">
                            <?php if ( has_post_thumbnail() ) : ?>
                                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                    <?php the_post_thumbnail(); ?>
                                </a>
                            <?php endif; ?>
                        </div>
                        <!-- <div class="image" style="background-image: url(<?php //echo get_the_post_thumbnail_url(); ?>);"></div> -->
                        <div class="overlay">
                            <div class="overlay-inner">
                                <h4><?php the_field('title'); ?></h4>
                                <h2><?php the_title(); ?></h2>
                                <?php
                                $description = get_the_content(); 
                                $description = substr($description,0, strpos($description, "</p>")+4);
                                ?>
                                <p><?php echo $description ?></p>
                                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                    <p class="read-more">
                                        <span>Узнать больше</span>
                                        <span class="icon" ></span>
                                    </p>
                                </a>
                            </div>
                            <div class="">
​
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; wp_reset_postdata(); ?>
    </div>
<?php endif; ?>