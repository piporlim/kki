<div class="home-partnerships" style="background-image: url('<?php the_field('background_img'); ?>');">
	<div class="container">
		<div class="partnership-title">
			<h2><?php the_field('b_section_name'); ?></h2>
		</div>
		<?php if( have_rows('benefits_repeater') ): ?>
		<div class="benefits-list">
			<?php while ( have_rows('benefits_repeater') ) : the_row(); ?>
				<div class="item-list">
					<div class="benefit-icon">
						<img src="<?php the_sub_field('icon'); ?>">
					</div>
					<div class="benefit-info">
						<h3><?php the_sub_field('title'); ?></h3>
						<p><?php the_sub_field('description'); ?></p>
					</div>
				</div>
			<?php endwhile; ?>
		</div>	
		<?php endif; ?>
	</div>
</div>