<div class="home-our-clients">
	<div class="container">
		<div class="our-client-title">
			<h2><?php the_field('c_section_name'); ?></h2>
		</div>
		<?php if(get_field('logo_slider') && have_rows('logo_slider')): ?>
		<div class="slider logo">
			<?php while(have_rows('logo_slider')): the_row(); ?>
			<div class="item">
				<img src="<?php the_sub_field('logo'); ?>">
			</div>
			<?php endwhile; ?>
		</div>
		<?php endif; ?>
	</div>
</div>

