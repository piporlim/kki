<div class="home-products">
	<div class="container">
		<div class="products-title">
			<h2><?php the_field('p_section_name'); ?></h2>
		</div>
		<?php if( have_rows('product_repeater') ): ?>
		<div class="product-list">
			<?php while ( have_rows('product_repeater') ) : the_row(); ?>
				<div class="item-list zoom">
					<div class="product-img">
						<img src="<?php the_sub_field('background_image'); ?>">
					</div>
					<div class="product-info">
						<p><?php the_sub_field('product_name'); ?></p>
						<a href="<?php the_sub_field('learn_more'); ?>"><?php the_sub_field('button_name'); ?><i class="fas fa-chevron-circle-right"></i></a>
					</div>
				</div>
			<?php endwhile; ?>
		</div>	
		<?php endif; ?>
	</div>
</div>