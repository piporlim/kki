<div class="home-hero">
	<div class="hero-bg">
		<img src="<?php the_field('background_image'); ?>">
	</div>
	<div class="container">
		<div class="hero-title">
			<h1><?php the_field('title'); ?></h1>
			<p><?php the_field('description'); ?></p>
			<button><a href="<?php the_field('learn_more'); ?>"><?php the_field('button_name'); ?></a></button>
		</div>	
	</div>
</div>

