<div class="wrap-contact">
	<div class="container">
            <div class="section-content">
                <div class="map-control">
                    <div class="map">
                        <?php ?>
                        
                        
                        <iframe src="<?php the_field('maps') ?>" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                        
                    </div>
                    <div class="info">
                        <div class="info-item">
                            <span class="info-icon">
                                <i class="fas fa-map-marker-alt"></i>
                            </span>
                            <div class="info-description">
                                <p><?php echo get_field('address-title'); ?></p>
                                <p><?php echo get_field('address'); ?></p>
                            </div>
                        </div>
                        <div class="info-item">
                            <span class="info-icon">
                                <i class="fas fa-envelope"></i>
                            </span>
                            <div class="info-description">
                                <p><?php echo get_field('email-title'); ?></p>
                                <p><?php echo get_field('email'); ?></p>
                            </div>
                        </div>
                        <div class="info-item">
                            <span class="info-icon">
                                <i class="fas fa-phone-alt"></i>
                            </span>
                            <div class="info-description">
                                <p><?php echo get_field('phone-title'); ?></p>
                                <p><?php echo the_field('phone-number'); ?></p>
                                <p><?php echo get_field('email'); ?></p>
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
                <div class="contact-form">
                    <div class="contact-form-control">
                        <h4> <?php echo get_field('contact-title'); ?></h4>
                        <p><?php echo get_field('contact-description'); ?></p>
                        <div class="contact">
                            <?php echo do_shortcode('[contact-form-7 id="68" title="contact-2"]')?> 
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>