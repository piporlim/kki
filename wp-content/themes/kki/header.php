<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package kki
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<!-- <a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'kki' ); ?></a> -->

	<header id="masthead" class="site-header container">
		<div class="site-branding">
			<?php
			the_custom_logo();
			 ?>	
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation">
			<!-- <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'kki' ); ?></button> -->
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
					'link_after' => '<span class="icon-drop"></span>',
				)
			);
			?>
			<div class="mk-request">
				<button type="button" >
					<a href="<?php echo get_site_url(); ?>/place-order/ " >Оформить заявку</a>
				</button>
			</div>
		</nav><!-- #site-navigation -->
		<div class="hamberger">
			<span class="hamberger-icon">&#9776;</span>
		</div>

		<div id="mobile" class="mobile-nav">
			<div class="close-hamberger">
				<span class="closebtn">&times;</span>
			</div>
			<div class="mobile-control">
				<?php
					wp_nav_menu(
						array(
							'theme_location' => 'menu-1',
							'menu_id'        => 'primary-menu',
							'link_after' => '<span class="icon-drop"></span>',
						)
					);
				?>
			</div>
		</div>
	</header><!-- #masthead -->
