<?php
/**
* Template Name: About Us
*
*/
get_header();

?>
<div class="site-wrap" id="site-wrap">
<?php
	include_once get_template_directory().'/components/section-banner.php';
	include_once get_template_directory().'/pages/about-us/section-history.php';
	include_once get_template_directory().'/pages/about-us/section-keypoint.php';
	include_once get_template_directory().'/pages/about-us/section-our-products.php';
	include_once get_template_directory().'/pages/about-us/section-preparation.php';
	include_once get_template_directory().'/pages/about-us/section-quality.php';
  get_footer();
?>
</div>