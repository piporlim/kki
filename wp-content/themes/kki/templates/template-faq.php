<?php
/**
* Template Name: F.A.Q
*
*/
get_header();

?>
<div class="site-wrap" id="site-wrap">
<?php
	include_once get_template_directory().'/components/section-banner.php';
	include_once get_template_directory().'/pages/F.A.Q/faq.php';
  get_footer();
?>
</div>