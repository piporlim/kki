<?php
/**
* Template Name: Place-order
*
*/
get_header();

?>
<div class="site-wrap" id="site-wrap">
<?php
	include_once get_template_directory().'/components/section-banner.php';
	include_once get_template_directory().'/pages/place-order/place-order.php';
  get_footer();
?>
</div>