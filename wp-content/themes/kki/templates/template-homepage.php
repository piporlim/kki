<?php
/**
* Template Name: Home Page
*
*/
get_header();

?>
<div class="site-wrap" id="site-wrap">
<?php
  include_once get_template_directory().'/pages/home/section-hero.php';
  include_once get_template_directory().'/pages/home/section-products.php';
  include_once get_template_directory().'/pages/home/section-partnerships.php';
  include_once get_template_directory().'/pages/home/section-our-clients.php';

  get_footer();
?>
</div>