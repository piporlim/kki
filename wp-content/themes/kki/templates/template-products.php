<?php
/**
* Template Name: Products
*
*/
get_header();

?>
<div class="site-wrap" id="site-wrap">
<?php
	include_once get_template_directory().'/components/section-banner.php';
	include_once get_template_directory().'/pages/products/products.php';
  get_footer();
?>
</div> 