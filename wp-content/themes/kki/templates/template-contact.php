<?php
/**
* Template Name: Contact
*
*/
get_header();

?>
<div class="site-wrap" id="site-wrap">
<?php
	include_once get_template_directory().'/components/section-banner.php';
	include_once get_template_directory().'/pages/contact/contact.php';
  get_footer();
?>
</div>