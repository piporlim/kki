<div class="contact-banner" style="background-image:url('<?php echo the_field('bg-img', 'option'); ?>');">
	<div class="container">
			<div class="section-banner">
          <h1><?php the_title(); ?></h1>
          <?php
            if ( function_exists('yoast_breadcrumb') ) {
              yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
					  }
				  ?>
      </div>
    </div>
</div>  