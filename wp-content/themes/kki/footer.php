<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package kki
 */

?>
	<footer id="colophon" class="site-footer">
		<div class="container">
			<div class="footer-1">
				<?php if(is_active_sidebar('footer-1')): ?>
					<?php dynamic_sidebar( 'footer-1' ); ?>
				<?php endif; ?>
			</div>
			<div class="footer-2">
				<?php if(is_active_sidebar('footer-2')): ?>
					<?php dynamic_sidebar( 'footer-2' ); ?>
				<?php endif; ?>
			</div>

			<div class="socail-icon">
				<div class="item-icon">
				<a href="<?php echo get_field('facebook', 'option'); ?>">
					<i class="fab fa-facebook-square"></i>
				</a>
				</div>
				<div class="item-icon">
					<a href="<?php echo get_field('instagram', 'option'); ?>">
						<i class="fab fa-instagram"></i>
					</a>
				</div>
			</div>
			<div class="cp-right">
				<p><?php echo get_field('copyright', 'option'); ?></p>
			</div>

			
		</div>


		<!-- <div class="site-info">
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'kki' ) ); ?>">
				<?php
				/* translators: %s: CMS name, i.e. WordPress. */
				printf( esc_html__( 'Proudly powered by %s', 'kki' ), 'WordPress' );
				?>
			</a>
			<span class="sep"> | </span>
				<?php
				/* translators: 1: Theme name, 2: Theme author. */
				printf( esc_html__( 'Theme: %1$s by %2$s.', 'kki' ), 'kki', '<a href="http://underscores.me/">Underscores.me</a>' );
				?>
		</div> -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
