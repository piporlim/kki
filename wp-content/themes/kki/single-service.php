<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package kki
 */
get_header();
?>

<main id="primary" class="site-main">
    <div class="site-wrap">
        <?php include_once get_template_directory().'/components/section-banner.php';?> 
    </div>
        <?php
        while ( have_posts() ) :
            the_post();
            // the_title();
            //the_field('title');
            // $gallery = get_field('gallery');
            
            // $images = get_field('gallery');           
        ?>
            <div class="single-service">
                <div class="container">
                    <div class="single-post">
                        <div class="single-service-slider">
                        <?php
                                $images = get_field('gallery');
                                
                                if( $images ): ?>
                                    <div class="slider-for">
                                        <?php foreach( $images as $image ): ?>
                                            <div  class="">
                                                
                                                    <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
                                               
                                                <p><?php echo $image['caption']; ?></p>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                    <div class="slider-nav">
                                        <?php foreach( $images as $image ): ?>
                                            <div  class="slide-control">
                                                
                                                    <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
                                                
                                                <p><?php echo $image['caption']; ?></p>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                <?php endif; ?>    
                        </div>
                        <div class="service">
                            <div class="service-control">
                                <h3><?php the_field('title'); ?></h3>
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            
            </div>  
            <?php endwhile; // End of the loop.?>
​    </main><!-- #main -->
​
<?php
get_footer();