import Header from './modules/header';
import Slider from "./modules/Slider";
import Tab from "./modules/producttab";
import Maps from './modules/maps';
import Accordion from './modules/accordion';
import Slidersyncing from './modules/slidersyncing';
import Input from './modules/input';
import Contact from './modules/contact';
import Zoom from './modules/zoom';

'use strict';
let header = new Header();
let slider = new Slider();
let producttab = new Tab();
let maps = new Maps();
let accordion = new Accordion();
let slidersyncing = new Slidersyncing();
let input = new Input();
let zoom = new Zoom();