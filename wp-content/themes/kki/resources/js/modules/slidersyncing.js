import $ from 'jquery';
import 'slick-carousel';
class Slidersyncing {
    constructor() {
        this._init();
    }
    _init() {
        $(function() {

        $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            fade: true,
            asNavFor: '.slider-nav'
        });
        
        $('.slider-nav').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            asNavFor: '.slider-for',
            dots: false,
            infinite: true,
            centerMode: true,
            focusOnSelect: true,
            prevArrow: '<a class="slick-prev" aria-label="Previous" type="button"></a>',
            nextArrow: '<a class="slick-next" aria-label="Next" type="button"></a>',
        });
      });
    }
}
export default Slidersyncing;