import $ from 'jquery';
import 'slick-carousel';
class Slider {
    constructor() {
        this._init();
    }
    _init() {
        $(function() {
        if($('.load-more').length>0 && $('.load-more-content').length>0){
            $('.load-more').on('click', function(e){
                e.preventDefault();
                if($(this).hasClass('loaded')){
                    $(this).text($(this).attr('data-more'));
                    $(this).removeClass('loaded');
                    $(this.parentNode).find('.load-more-content').slideUp();
                }
                else{
                    $(this).addClass('loaded');
                    $(this).text($(this).attr('data-less'));
                    $(this.parentNode).find('.load-more-content').slideDown();
                }
            });
        }
        $(".slider").slick({
            // normal options...
            slidesToShow: 1,
            autoplay: true,
            autoplaySpeed: 2000,
            infinite: true,
            arrows: false,
            dots: true,
        });

      });
    }
}
export default Slider;