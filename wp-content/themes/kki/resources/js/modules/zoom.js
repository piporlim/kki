import $ from 'jquery';

class Zoom {

    constructor() {
        this._init();
    }
    
    _init() {
        $(document).ready(function(){
        	$('.zoom').mouseenter(function() {
        		$(this).find('img').css({"width":"110%", "height":"110%","transition":"0.5s"});
        	});
        	$('.zoom').mouseleave(function() {
        		$(this).find('img').css({"width":"100%", "height":"100%","transition":"0.5s"});
        	});
        });
    }
}

export default Zoom;
