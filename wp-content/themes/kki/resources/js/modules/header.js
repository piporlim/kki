import $ from 'jquery';

class Header {

    constructor() {
        this._init();
    }
    
    _init() {
        $(function() {

            $('.hamberger-icon').on('click', function() {
                $('#mobile').css({
                    'height': '100%'
                });
            });

            $('.closebtn').on('click', function() {
                $('#mobile').css({
                    'height': '0%'
                });
            });

            $('.menu-item').on('click', function(e) {
                // e.preventDefault();
                console.log(e.target);
                $(".sub-menu").hide(); 
                $(this).find(".sub-menu").show();
                // e.stopPropagation();
            });
            

        });
    }
}

export default Header;