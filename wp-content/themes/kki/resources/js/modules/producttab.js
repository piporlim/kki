import $ from 'jquery';

class Tab {
    constructor() {
        this._init();
    }
    _init() {
        $(function() {    
            $('.tablinks-1').on('click', function() {
                // evt.preventDefault();
                if ($('.tablinks-1').hasClass("active")) {

                    $('.tablinks-1').addClass("active");
                    $('.tabcontent-1').addClass("active");
                } else {
                    $('.tablinks-2').removeClass("active");
                    $('.tabcontent-2').css("display","none");
                    $('.tablinks-1').addClass("active");
                    $('.tabcontent-1').css("display","block");
                }
            }); 
            $('.tablinks-2').on('click', function() {
                $('.tablinks-1').removeClass("active");
                $('.tabcontent-1').css("display","none");
                $('.tablinks-2').addClass("active");
                $('.tabcontent-2').css("display","block");
            }); 
        });
    }
}

export default Tab;