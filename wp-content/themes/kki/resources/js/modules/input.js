import $ from 'jquery';

class Input {

    constructor() {
        this._init();
    }
    
    _init() {
        $(document).ready(function(){

            $('.place-order').click(function(){
                $(this).addClass('active');
                $(this).find('input').focus();
                $(this).removeClass('error filled');
            });

            $('.place-order').focusout(function(){
                if ($(this).find('.wpcf7-form-control').val() ) {
                    $(this).removeClass('active');
                    $(this).addClass('filled');
                }
                else {
                    $(this).addClass('error');
                }
            });

            // $('.wpcf7').on('wpcf7invalid wpcf7spam wpcf7mailsent wpcf7mailfailed wpcf7submit', function(event){
            $('.wpcf7').on('wpcf7submit', function(e) {
                $('input').each( function() {
                    if ( ! $(this).val() ) {
                        $(this).parents('.place-order').addClass('error');
                    }
                    else {
                        $(this).parents('.place-order').removeClass('error');
                    }
                });
                if ( ! $('textarea').val() ) {
                    $('textarea').parents('.place-order').addClass('error');
                }
                else {
                    $('textarea').parents('.place-order').removeClass('error');
                }   

            });
        });
    }
}

export default Input;
