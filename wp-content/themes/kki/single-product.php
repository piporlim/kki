<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package kki
 */

get_header();
?>
    <div class="site-wrap">
        <?php include_once get_template_directory().'/components/section-banner.php';?> 
    </div>
	<main id="primary" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();
            // the_title();
			// the_fied('product-description');
        ?>    
        <div class="single-product">
            <div class="container">
                <div class="products">
                    <div class="single-product-slider">
                    <?php
                            $images = get_field('product-gallery');
                            
                            if( $images ): ?>
                                <div class="slider-for">
                                    <?php foreach( $images as $image ): ?>
                                        <div  class="">
                                            
                                                <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
                                            
                                            <p><?php echo $image['caption']; ?></p>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                                <div class="slider-nav">
                                    <?php foreach( $images as $image ): ?>
                                        <div  class="slide-control">
                                            
                                                <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
                                            
                                            <p><?php echo $image['caption']; ?></p>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            <?php endif; ?>    
                    </div>
                    <div class="product">
                        <div class="product-control">
                             <h3><?php the_field('product-title'); ?></h3>
                            <div class="tab">
                                <div class="tablinks-1 active" ><?php the_field('product-desc-title'); ?></div>
                                <div class="tablinks-2" ><?php the_field('characteristics-title'); ?></div>
                            </div>
                            <div class="product-desc tabcontent-1 active" >
                                <div class="desc-control">
                                    <p><?php the_content(); ?></p>
                                    
                                </div>
                            </div>
                            <div class="characteristics tabcontent-2">
                                <div class="charac-control">
                                    
                                    <div class="wrap-charac">
                                        <?php

                                        // Check rows exists.
                                        if( have_rows('characteristics') ):

                                            // Loop through rows.
                                            while( have_rows('characteristics') ) : the_row();
                                            ?>  
                                            <div class="wrap-feature-title">
                                                <p><?php the_sub_field('product-feature-title'); ?></p>

                                            </div>

                                            <div class="wrap-detail">
                                            <?php

                                                // Check rows exists.
                                                if( have_rows('product-detail') ):

                                                    // Loop through rows.
                                                    while( have_rows('product-detail') ) : the_row();
                                                    ?> 
                                                        <div class="wrap-detail-control">
                                                            <div class="detail-title">
                                                                <div class="detail-control">
                                                                    <p><?php the_sub_field('detail-title'); ?></p>
                                                                </div>
                                                            </div> 
                                                            
                                                            <div class="detail-value">
                                                                <div class="detail-control">
                                                                    <p><?php the_sub_field('detail-value'); ?></p>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php 
                                                // Do something... 
                                                // End loop.
                                            endwhile;
                                            
                                            // No value.
                                            else :
                                                // Do something...
                                            endif;
                                            ?>  
                                            </div> 
                                        <?php 
                                        // Do something... 
                                            // End loop.
                                            endwhile;
                                        
                                        // No value.
                                        else :
                                            // Do something...
                                        endif;
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section-products">
                    <div class="product-img1" style="background-image:url('<?php echo the_field('single-product-image'); ?>');">
                        <div class="pdt">
                            <h3><?php echo the_field('section-pdt-tile'); ?></h3>
                            <h1><?php echo the_field('section-pdt-value'); ?></h1>
                        </div>
                    </div>
                    <div class="product-img2">
                        <img src="<?php echo the_field('product-img2', 'option'); ?>" alt="">
                        <a href="<?php echo get_site_url(); ?>/place-order/">
                            <div class="read-more">
                                <h3>Оформить заявку</h3>
                                <span>Узнать больше</span>
                                <span class="icon" ></span>
                            </div>
                        </a>
                    </div>
                </div>  
            </div>
        </div>  

			
        <?php    
		endwhile; // End of the loop.
		?>


	</main><!-- #main -->

<?php
get_footer();
